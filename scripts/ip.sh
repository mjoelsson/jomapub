#!/bin/bash

HOSTS=(
    "localhost"
    "8.8.8.8"
    "31.13.72.36"
    "2.18.34.184"
    "52.223.48.227"
)

for addr in "${HOSTS[@]}"; do
    echo "Pinging $addr" 
    ping -c "2" "$addr"

    if [ $? -ne "0" ]; then
       echo "$addr is not available"
      exit 1
    fi
done
